package com.project.UIUniversityLibrary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UiUniversityLibraryApplication {

	public static void main(String[] args) {
		SpringApplication.run(UiUniversityLibraryApplication.class, args);
	}

}
