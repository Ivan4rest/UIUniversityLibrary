package com.project.UIUniversityLibrary.dto;

import lombok.Data;

import java.sql.Date;

@Data
public class FileMetadataDTO {
    Long id;
    Date addedDate;
    String userUUID;
    Boolean visibility;
    Long directoryId;
    String directoryPath;
    String filename;

    public FileMetadataDTO() {
    }

    public FileMetadataDTO(Long id, Date addedDate, String userUUID, Boolean visibility, Long directoryId, String directoryPath, String filename) {
        this.id = id;
        this.addedDate = addedDate;
        this.userUUID = userUUID;
        this.visibility = visibility;
        this.directoryId = directoryId;
        this.directoryPath = directoryPath;
        this.filename = filename;
    }
}
