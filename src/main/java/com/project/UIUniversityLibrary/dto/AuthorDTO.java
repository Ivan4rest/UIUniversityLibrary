package com.project.UIUniversityLibrary.dto;

import lombok.Data;

import java.sql.Date;
import java.util.List;

@Data
public class AuthorDTO {
    Long id;
    String firstName;
    String lastName;
    String patronymic;
    Date birthDate;
    List<Long> booksId;

    public AuthorDTO() {
    }

    public AuthorDTO(String firstName, String lastName, String patronymic, Date birthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.birthDate = birthDate;
    }
}
