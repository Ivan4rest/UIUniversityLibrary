package com.project.UIUniversityLibrary.dto;

import lombok.Data;

import java.util.List;

@Data
public class GenreDTO {
    Long id;
    String name;
    List<Long> booksId;

    public GenreDTO() {
    }

    public GenreDTO(String name) {
        this.name = name;
    }
}
