package com.project.UIUniversityLibrary.dto;

import lombok.Data;

import java.sql.Date;

@Data
public class FileDTO {
    Long id;
    Date addedDate;
    String userUUID;
    Boolean visibility;
    Long directoryId;
    String directoryPath;
    String filename;
    byte[] fileInBytes;

    public FileDTO() {
    }

    public FileDTO(Date addedDate, Boolean visibility, String directoryPath, String filename, byte[] fileInBytes) {
        this.addedDate = addedDate;
        this.visibility = visibility;
        this.directoryPath = directoryPath;
        this.filename = filename;
        this.fileInBytes = fileInBytes;
    }

    public FileDTO(Date addedDate, Boolean visibility, Long directoryId, String directoryPath, String filename, byte[] fileInBytes) {
        this.addedDate = addedDate;
        this.visibility = visibility;
        this.directoryId = directoryId;
        this.directoryPath = directoryPath;
        this.filename = filename;
        this.fileInBytes = fileInBytes;
    }

    public FileDTO(Date addedDate, String userUUID, Boolean visibility, String directoryPath, String filename, byte[] fileInBytes) {
        this.addedDate = addedDate;
        this.userUUID = userUUID;
        this.visibility = visibility;
        this.directoryPath = directoryPath;
        this.filename = filename;
        this.fileInBytes = fileInBytes;
    }

    public FileDTO(Date addedDate, String userUUID, Boolean visibility, Long directoryId, String directoryPath, String filename, byte[] fileInBytes) {
        this.addedDate = addedDate;
        this.userUUID = userUUID;
        this.visibility = visibility;
        this.directoryId = directoryId;
        this.directoryPath = directoryPath;
        this.filename = filename;
        this.fileInBytes = fileInBytes;
    }
}
