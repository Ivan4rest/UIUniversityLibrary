package com.project.UIUniversityLibrary.dto;

import lombok.Data;

import java.sql.Date;
import java.util.List;

@Data
public class BookDTO {
    Long id;
    String title;
    Date publicationYear;
    String language;
    Long fileId;
    Long directoryId;
    Integer numberOfPages;
    List<Long> authorsId;
    List<Long> genresId;

    public BookDTO() {
    }

    public BookDTO(String title, Date publicationYear, String language, Long fileId, Long directoryId, Integer numberOfPages, List<Long> authorsId, List<Long> genresId) {
        this.title = title;
        this.publicationYear = publicationYear;
        this.language = language;
        this.fileId = fileId;
        this.directoryId = directoryId;
        this.numberOfPages = numberOfPages;
        this.authorsId = authorsId;
        this.genresId = genresId;
    }
}
