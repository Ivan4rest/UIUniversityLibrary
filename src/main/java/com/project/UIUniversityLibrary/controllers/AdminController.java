package com.project.UIUniversityLibrary.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.UIUniversityLibrary.dto.*;
import org.apache.commons.codec.binary.Base64;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class AdminController {

    @Value("${microservices.host}")
    private String hostMicroservice;

    @Value("${microservices.universitylibrary}")
    private String universityLibraryMicroservice;

    @Value("${microservices.filesharing}")
    private String fileSharingMicroservice;

    @GetMapping("/addBook")
    public String getAddBookPage(){
        return "addBook";
    }

//    @PostMapping("/addBook")
//    public String addBook(@RequestParam String title,
//                          @RequestParam String language,
//                          @RequestParam Date publicationDate,
//                          @RequestParam List<Long> authors,
//                          @RequestParam List<Long> genres,
//                          @RequestParam MultipartFile multipartFile) throws IOException {
//        RestTemplate restTemplate = new RestTemplate();
//        BookDTO bookDTO = new BookDTO(title, publicationDate, language, multipartFile.getOriginalFilename(), authors, genres);
//        HttpEntity<BookDTO> bookDTOEntity = new HttpEntity<>(bookDTO);
//        restTemplate.postForEntity(hostMicroservice + ":" + universityLibraryMicroservice + "/books", bookDTOEntity, Object.class);
//
//        FileDTO fileDTO = new FileDTO("books", multipartFile.getOriginalFilename(), multipartFile.getBytes());
//        HttpEntity<FileDTO> fileDTOEntity = new HttpEntity<>(fileDTO);
//        restTemplate.postForEntity(hostMicroservice + ":" + fileSharingMicroservice + "/files", fileDTOEntity, Object.class);
//        return "addBook";
//    }

    @PostMapping("/addBook")
    public String addBook(@RequestParam String title,
                          @RequestParam String language,
                          @RequestParam Date publicationDate,
                          @RequestParam String authors,
                          @RequestParam String genres,
                          @RequestParam MultipartFile multipartFile,
                          HttpServletRequest request) throws IOException {
        List<Long> authorsList = new ArrayList<>();
        List<Long> genresList = new ArrayList<>();
        for (int i = 0; i < authors.split(" ").length; i++) {
            authorsList.add(Long.parseLong(authors.split(" ")[i]));
        }
        for (int i = 0; i < genres.split(" ").length; i++) {
            genresList.add(Long.parseLong(genres.split(" ")[i]));
        }

        RestTemplate restTemplate = new RestTemplate();

        String directory = "books/" + multipartFile.getOriginalFilename();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));

        Date currentDate = new Date(new java.util.Date().getTime());

        FileDTO fileDTO = new FileDTO(currentDate, true, directory, multipartFile.getOriginalFilename(), multipartFile.getBytes());
        HttpEntity<FileDTO> fileDTOEntity = new HttpEntity<>(fileDTO, headers);
        ResponseEntity<FileDTO> responseAddPDF = restTemplate.exchange(hostMicroservice + ":" + fileSharingMicroservice + "/api/v0/files", HttpMethod.POST, fileDTOEntity, FileDTO.class);
        Long fileId = responseAddPDF.getBody().getId();
        Long directoryId = responseAddPDF.getBody().getDirectoryId();
        PDDocument document = PDDocument.load(multipartFile.getBytes());
        Integer numberOfPages = document.getNumberOfPages();
        PDFRenderer pdfRenderer = new PDFRenderer(document);
        for (int i = 0; i < document.getNumberOfPages(); i++) {
            BufferedImage bim = pdfRenderer.renderImageWithDPI(i, 300, ImageType.RGB);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bim, "png", baos);
            byte[] bytes = baos.toByteArray();
            FileDTO fileDTOImg = new FileDTO(currentDate, true, directory, i + ".png", bytes);
            HttpEntity<FileDTO> fileDTOEntityImg = new HttpEntity<>(fileDTOImg, headers);
            ResponseEntity<FileDTO> responseAddImg = restTemplate.exchange(hostMicroservice + ":" + fileSharingMicroservice + "/api/v0/files", HttpMethod.POST, fileDTOEntityImg, FileDTO.class);
        }
        document.close();
        BookDTO bookDTO = new BookDTO(title, publicationDate, language, fileId, directoryId, numberOfPages, authorsList, genresList);
        HttpEntity<BookDTO> bookDTOEntity = new HttpEntity<>(bookDTO, headers);
        ResponseEntity<String> response = restTemplate.exchange(hostMicroservice + ":" + universityLibraryMicroservice + "/api/v0/books", HttpMethod.POST, bookDTOEntity, String.class);

        return "addBook";
    }

    @GetMapping("/addAuthor")
    public String getAddAuthorPage(){
        return "addAuthor";
    }

    @PostMapping("/addAuthor")
    public String addAuthor(@RequestParam String firstName,
                            @RequestParam String lastName,
                            @RequestParam String patronymic,
                            @RequestParam Date birthDate,
                            HttpServletRequest request){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        AuthorDTO authorDTO = new AuthorDTO(firstName, lastName, patronymic, birthDate);
        HttpEntity<AuthorDTO> entity = new HttpEntity<AuthorDTO>(authorDTO, headers);
        ResponseEntity<String> response = restTemplate.exchange(hostMicroservice + ":" + universityLibraryMicroservice + "/api/v0/authors", HttpMethod.POST, entity, String.class);
        return "addAuthor";
    }

    @GetMapping("/addGenre")
    public String getAddGenrePage(){
        return "addGenre";
    }

    @PostMapping("/addGenre")
    public String addGenre(@RequestParam String genreName,
                           HttpServletRequest request){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        GenreDTO genreDTO = new GenreDTO(genreName);
        HttpEntity<GenreDTO> entity = new HttpEntity<GenreDTO>(genreDTO, headers);
        ResponseEntity<String> response = restTemplate.exchange(hostMicroservice + ":" + universityLibraryMicroservice + "/api/v0/genres", HttpMethod.POST, entity, String.class);
        return "addGenre";
    }

    @GetMapping("/findBook")
    public String getBooks(Model model,
                           HttpServletRequest request) throws JsonProcessingException {
        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response = template.exchange(hostMicroservice + ":" + universityLibraryMicroservice + "/api/v0/books", HttpMethod.GET, entity, String.class);
        ObjectMapper mapper = new ObjectMapper();
        List<BookDTO> bookDTOS = Arrays.asList(mapper.readValue(response.getBody(), BookDTO[].class));
        model.addAttribute("books", bookDTOS);

        return "findBook";
    }

    @GetMapping("/findAuthor")
    public String getAuthors(Model model,
                             HttpServletRequest request) throws JsonProcessingException {
        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response = template.exchange(hostMicroservice + ":" + universityLibraryMicroservice + "/api/v0/authors", HttpMethod.GET, entity, String.class);
        ObjectMapper mapper = new ObjectMapper();
        List<AuthorDTO> authorDTOS = Arrays.asList(mapper.readValue(response.getBody(), AuthorDTO[].class));
        model.addAttribute("authors", authorDTOS);

        return "findAuthor";
    }

    @GetMapping("/findGenre")
    public String getGenres(Model model,
                            HttpServletRequest request) throws JsonProcessingException {
        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response = template.exchange(hostMicroservice + ":" + universityLibraryMicroservice + "/api/v0/genres", HttpMethod.GET, entity, String.class);
        ObjectMapper mapper = new ObjectMapper();
        List<GenreDTO> genreDTOS = Arrays.asList(mapper.readValue(response.getBody(), GenreDTO[].class));
        model.addAttribute("genres", genreDTOS);

        return "findGenre";
    }

    @PostMapping("/deleteGenre/{id}")
    public String deleteGenre(@PathVariable Long id,
                              HttpServletRequest request){
        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        template.exchange(hostMicroservice + ":" + universityLibraryMicroservice + "/api/v0/genres/" + id, HttpMethod.DELETE, entity, String.class);
        return "findGenre";
    }

    @PostMapping("/deleteAuthor/{id}")
    public String deleteAuthor(@PathVariable Long id,
                               HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        RestTemplate template = new RestTemplate();
        ResponseEntity<String> response = template.exchange(hostMicroservice + ":" + universityLibraryMicroservice + "/api/v0/authors/" + id, HttpMethod.DELETE, entity, String.class);
        return "findAuthor";
    }

    @PostMapping("/deleteBook/{id}")
    public String deleteBook(@PathVariable Long id,
                             @RequestParam Long directoryId,
                             HttpServletRequest request){
        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<String> response = template.exchange(hostMicroservice + ":" + universityLibraryMicroservice + "/api/v0/books/" + id, HttpMethod.DELETE, entity, String.class);
        ResponseEntity<String> response2 = template.exchange(hostMicroservice + ":" + fileSharingMicroservice + "/api/v0/directories/" + directoryId, HttpMethod.DELETE, entity, String.class);
        return "findBook";
    }


    @GetMapping("/books/{id}/pages/{numberPage}")
    public String viewBook(@PathVariable Long id,
                           @PathVariable Integer numberPage,
                           Model model,
                           HttpServletRequest request) throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<BookDTO> responseBookDTO = restTemplate.exchange(hostMicroservice + ":" + universityLibraryMicroservice + "/api/v0/books/" + id, HttpMethod.GET, entity, BookDTO.class);
        BookDTO bookDTO = responseBookDTO.getBody();
        ResponseEntity<FileDTO> responseFileDTO = restTemplate.exchange(hostMicroservice + ":" + fileSharingMicroservice + "/api/v0/directories/" + bookDTO.getDirectoryId() + "/filenames/" + numberPage + ".png", HttpMethod.GET, entity, FileDTO.class);
        FileDTO fileDTO = responseFileDTO.getBody();
        byte[] imgBytesAsBase64 = Base64.encodeBase64(fileDTO.getFileInBytes());
        String imgDataAsBase64 = new String(imgBytesAsBase64);
        String imgAsBase64 = "data:image/png;base64," + imgDataAsBase64;
        model.addAttribute("page", imgAsBase64);
        model.addAttribute("bookId", id);
        model.addAttribute("currentNumberPage", numberPage);
        model.addAttribute("totalNumberPage", bookDTO.getNumberOfPages());
        return "viewBook";
    }

//    @GetMapping("/Books/{id}/{numberPage}")
//    public String viewBook(@PathVariable Long id,
//                           @PathVariable Integer numberPage,
//                           Model model) throws IOException {
//        RestTemplate restTemplate = new RestTemplate();
//        ResponseEntity<BookDTO> responseBookDTO = restTemplate.getForEntity(hostMicroservice + ":" + universityLibraryMicroservice + "/books/" + id, BookDTO.class);
//        BookDTO bookDTO = responseBookDTO.getBody();
//        ResponseEntity<FileDTO> responseFileDTO = restTemplate.getForEntity(hostMicroservice + ":" + fileSharingMicroservice + "/files/books/" + bookDTO.getFilename(), FileDTO.class);
//        FileDTO fileDTO = responseFileDTO.getBody();
//        PDDocument document = PDDocument.load(fileDTO.getFileInBytes());
//        if((numberPage < 0) || (numberPage >= document.getNumberOfPages())){
//            numberPage = 0;
//        }
//        PDDocument bufferDocument = new PDDocument();
//        bufferDocument.addPage(document.getPage(numberPage));
//        PDFRenderer pdfRenderer = new PDFRenderer(bufferDocument);
//        BufferedImage bim = pdfRenderer.renderImageWithDPI(0, 300, ImageType.RGB);
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        ImageIO.write(bim, "png", baos);
//        byte[] bytes = baos.toByteArray();
//        byte[] imgBytesAsBase64 = Base64.encodeBase64(bytes);
//        String imgDataAsBase64 = new String(imgBytesAsBase64);
//        String imgAsBase64 = "data:image/png;base64," + imgDataAsBase64;
//        model.addAttribute("page", imgAsBase64);
//        model.addAttribute("bookId", id);
//        model.addAttribute("currentNumberPage", numberPage);
//        model.addAttribute("totalNumberPage", document.getNumberOfPages());
//        document.close();
//        bufferDocument.close();
//        return "viewBook";
//    }

//    @GetMapping("/test/{filename}")
//    public ResponseEntity<byte[]> getPDF1(@PathVariable String filename) {
//
//        RestTemplate restTemplate = new RestTemplate();
//        ResponseEntity<FileDTO> response = restTemplate.getForEntity(hostMicroservice + ":" + fileSharingMicroservice + "/files/books/" + filename, FileDTO.class);
//        FileDTO fileDTO = response.getBody();
//        byte[] fileInBytes = fileDTO.getFileInBytes();
//
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.parseMediaType("application/pdf"));
//
//        headers.add("content-disposition", "inline;filename=" + filename);
//
//        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//        ResponseEntity<byte[]> responseBytes = new ResponseEntity<byte[]>(fileInBytes, headers, HttpStatus.OK);
//        return responseBytes;
//    }

    private String getToken(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getTokenString();
    }
}